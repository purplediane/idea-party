
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views import static

import idea_party.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^static/(?P<path>.*)$', static.serve, {'document_root': settings.STATIC_ROOT}),
    url(r'^media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^$', idea_party.views.index, name='index'),
    url(r'^auth/', include('idea_party.auth.urls', namespace='auth')),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
]
